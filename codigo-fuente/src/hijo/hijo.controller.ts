import {Controller} from '@nestjs/common';
import {HijoService} from "./hijo.service";

@Controller('hijo')
export class HijoController {
    constructor(
        private readonly _hijoService: HijoService
    ) {
    }
}
