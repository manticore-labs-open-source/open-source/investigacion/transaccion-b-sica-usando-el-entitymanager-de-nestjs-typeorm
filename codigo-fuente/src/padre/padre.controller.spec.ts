import { Test, TestingModule } from '@nestjs/testing';
import { PadreController } from './padre.controller';

describe('Padre Controller', () => {
  let controller: PadreController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [PadreController],
    }).compile();

    controller = module.get<PadreController>(PadreController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
